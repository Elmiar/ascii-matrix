#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>//for exit() function
int compareTwoString(char *, char *);
void print_help()
{
	printf("ASCII-MATRIX\n\nThis script written in C will render the matrix effect in the \nterminal, while rendering ascii art loaded from a txt file, \nat the center of the window.\n\nHOW TO CREATE SUITABLE ASCII-ART txt FILES\n\nThis program renders ascii-art txt files that contain these characters: (white space),1,2,3.\n\nIn order to produce this kind of txt files, the program jp2a is used\n(https://github.com/cslarsen/jp2a).\n\nTo install this program in Debian-base OSs:\n\napt-get install jp2a\n\nTo generate an ascii-art txt file from a png image, in order\nto use it with ascii-matrix, run the following command:\n\njp2a path/to/image.png --chars= 123 --height=20>path/to/output.txt\n\nUSAGE\n\nWhile in the ascii-matrix/ directory, and once you have\ngenerated the ascii-art txt file that you want to show,run:\n\n\t./ascii-matrix -f path/to/ascii.txt\n\nYou may want to add the flags that you prefer in order to have an outcome of your liking.\n\nFLAGS\n\nThe user can either use the short or the long flag version\n(i.e. ascii-matrix -h and ascii-matrix --help are the same).\n\n-h,--help\tShows this help text.\n\n-f,--file\tDefines the path to the ascii txt file to render in the center\n\t\tof the matrix window.\n\t\tIf this flag isn't used, the command will still render\n\t\ta matrix screen with no ascii art.\n\n-s,--speed\tDefines the speed of the matrix falling digits.\n\t\tAcceptable values 0-9 (default: 5).\n\n-d,--dense\tDefines the density of the matrix lines.\n\t\tAcceptable values 0-9 (default: 5).\n\n-m1,--matrix-color1\tDefines the color of first matrix digits \n\t\t(default: yellow).\n\n-m2,--matrix-color2\tDefines the color of matrix digits \n\t\t(default: green).\n\n-c1,--color-ascii1 : The first color of the ascii art (default: blue).\n\n-c2,--color-ascii2 : The second color of the ascii art (default: red).\n\n-c3,--color-ascii3 : The third color of the ascii art (default: white).\n\n\t\tAcceptable color values :black, maroon, green, olive, navy, \n\t\tpurple, teal, silver, grey, red, lime, yellow, blue, fuchsia, \n\t\taqua and white.\n\n-i,--invert : Inverts the colors of the ascii art (background-foreground).\n\n-a1,--ascii1 : Defines the characters for the first category of the ascii art (default: L).\n\n-a2,--ascii2 : Defines the characters for the second category of the ascii art (default: L).\n\n-a3,--ascii3 : Defines the characters for the third category of the ascii art (default: L).\n\n\tAcceptable -a1 -a2 -a3 values :\n\t\tL Random uppercase letters [A-Z]\n\t\tl Random lowercase letters [a-z]\n\t\tn Random numbers [0-9]\n\t\ts Space (best used in combination with -i or -d 9)\n\t\tX the letter X\n\t\tp Random punctuation marks '()*+,-./\n\nSome sample ascii txt files were added to the repo, \njust to help with examples of usage.\n\nEXAMPLE 1\n\n./ascii-matrix \n\nwill render a matrix window with the default matrix colors.\n\nEXAMPLE 2\n\n./ascii-matrix -f ubuntu.txt \n\nwill render the ubuntu.txt ascii, with the default values.\n\nEXAMPLE 3\n\n./ascii-matrix -f xfce.txt -c1 grey -c2 aqua -c3 white -a1 l -a2 n -a3 L -m1 yellow -m2 green\n\nwill render the xfce.txt ascii, with white uppercase letters,\ngrey lowercase letters and aqua punctuation marks, in a matrix\nwith yellow and green digits.\n\nEXAMPLE 4\n\n./ascii-matrix -f archlinux.txt -c1 navy -c2 aqua -c3 grey -a1 s -a2 s -a3 s -i\n\nwill render the archlinux.txt ascii in spaces,with the defined colors inverted.\n\nEXAMPLE 5\n\n./ascii-matrix -f mint1.txt -d 9 -a1 s -a2 s -a3 s\n\nwill render the mint1.txt ascii with spaces, in a fully dense lined matrix screen.\n\nEXAMPLE 6\n\n./ascii-matrix -f fedora.txt -c1 aqua -a1 L -a2 p -a3 s  -s 8\n\nwill render the fedora.txt ascii in aqua, in a rapid matrix screen.\n\nEXAMPLE 7\n\n./ascii-matrix -f mint2.txt -c1 grey -c2 green -a1 s -a2 s -i\n\nwill render the mint2.txt ascii in green and grey spaces inverted.\n");
	exit(0);
	}
int get_color_code(char *color, int code)
{
	int x,def;
	def=code;
	char colors[16][10]={"black","maroon","green","olive","navy","purple","teal","silver","grey","red","lime","yellow","blue","fuchsia","aqua","white"};
	for(x=0;x<16;x++)
	{
		int compare = compareTwoString(color, colors[x]);
		  if (compare == 0)
  {
			def=x;
   break;
  }
	}
   return(def);
}//get_color_code
int get_char_code(char *character, int code)
{
	int x,def;
	def=rand() % 26 + 65;
	char characters[6][2]={"L","l","n","s","X","p"};
	int codes[6];
	codes[0]=rand() % 26 + 65;
	codes[1]=rand() % 26 + 97;
	codes[2]=rand() % 10 + 48;
	codes[3]=32;
	codes[4]=88;
	codes[5]=rand() % 9 + 39;
	for(x=0;x<6;x++)
	{
		int compare = compareTwoString(character, characters[x]);
		  if (compare == 0)
  {
			def=codes[x];
   break;
  }
	}
   return(def);
}//get_char_code
int get_int_code(char *val, int code)
{
	int x,def;
	def=code;
	char vals[10][2]={"0","1","2","3","4","5","6","7","8","9"};
	for(x=0;x<10;x++)
	{
		int compare = compareTwoString(val,vals[x]);
		  if (compare == 0)
  {
			def=x;
   break;
  }
	}
   return(def);
}//get_int_code
int main(int argc, char **argv)
{
 //variables declaration
 FILE *fptr;
 char *flag_h="-h";
 char *flag_help="--help";
 char *flag_f="-f";
 char *flag_file="--file";
 char *flag_s="-s";
 char *flag_speed="--speed";
 char *flag_i="-i";
 char *flag_invert="--invert";
 char *flag_d="-d";
 char *flag_dense="--dense";
 char *flag_m1="-m1";
 char *flag_matrix_color1="--matrix-color1";
 char *flag_m2="-m2";
 char *flag_matrix_color2="--matrix-color2";
 char *flag_c1="-c1";
 char *flag_color_ascii1="--color-ascii1";
 char *flag_c2="-c2";
 char *flag_color_ascii2="--color-ascii2";
 char *flag_c3="-c3";
 char *flag_color_ascii3="--color-ascii3";
 char *flag_a1="-a1";
 char *flag_ascii1="--ascii1";
 char *flag_a2="-a2";
 char *flag_ascii2="--ascii2";
 char *flag_a3="-a3";
 char *flag_ascii3="--ascii3";
 char *m1="",*m2="",*c1="",*c2="",*c3="",*speed="",*invert="3",*dense="",*a1="",*a2="",*a3="";
	int m1c=11,m2c=2,c1c=12,c2c=9,c3c=15,speedc=5,densec=5;
	int a1c=rand() % 26 + 65;
	int a2c=rand() % 26 + 65;
	int a3c=rand() % 26 + 65;
 char *filename;
 int x,y,ch,t,i,ii=0,columnmargins,rowmargins,columnmarginleft,rowmargintop;
 int asciirow=0;
 int asciicolumn=0;
 //determining argument of -f flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_f); // calling compareTwoString() function.
  if (compare == 0)
  {
   filename=argv[t+1];
  }
 }
 //determining argument of --file flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_file); // calling compareTwoString() function.
  if (compare == 0)
  {
   filename=argv[t+1];
  }
 }
 //determining argument of -s flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_s); // calling compareTwoString() function.
  if (compare == 0)
  {
   speed=argv[t+1];
  }
 }
 //determining argument of --speed flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_speed); // calling compareTwoString() function.
  if (compare == 0)
  {
   speed=argv[t+1];
  }
 }
 //determining argument of --help flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_help); // calling compareTwoString() function.
  if (compare == 0)
  {
   print_help();
  }
 }
 //determining argument of -h flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_h); // calling compareTwoString() function.
  if (compare == 0)
  {
   print_help();
  }
 }
 //determining argument of -d flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_d); // calling compareTwoString() function.
  if (compare == 0)
  {
   dense=argv[t+1];
  }
 }
 //determining argument of --dense flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_dense); // calling compareTwoString() function.
  if (compare == 0)
  {
   dense=argv[t+1];
  }
 }
 //determining argument of --invert flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_invert); // calling compareTwoString() function.
  if (compare == 0)
  {
   invert="4";
  }
 }
 //determining argument of -i flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_i); // calling compareTwoString() function.
  if (compare == 0)
  {
   invert="4";
  }
 }
 //determining argument of --matrix-color1 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_matrix_color1); // calling compareTwoString() function.
  if (compare == 0)
  {
   m1=argv[t+1];
  }
 }
 //determining argument of -m1 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_m1); // calling compareTwoString() function.
  if (compare == 0)
  {
   m1=argv[t+1];
  }
 }
  //determining argument of --matrix-color2 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_matrix_color2); // calling compareTwoString() function.
  if (compare == 0)
  {
   m2=argv[t+1];
  }
 }
 //determining argument of -m2 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_m2); // calling compareTwoString() function.
  if (compare == 0)
  {
   m2=argv[t+1];
  }
 }
  //determining argument of --color-ascii1 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_color_ascii1); // calling compareTwoString() function.
  if (compare == 0)
  {
   c1=argv[t+1];
  }
 }
 //determining argument of -c1 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_c1); // calling compareTwoString() function.
  if (compare == 0)
  {
   c1=argv[t+1];
  }
 }
  //determining argument of --color-ascii2 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_color_ascii2); // calling compareTwoString() function.
  if (compare == 0)
  {
   c2=argv[t+1];
  }
 }
 //determining argument of -c2 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_c2); // calling compareTwoString() function.
  if (compare == 0)
  {
   c2=argv[t+1];
  }
 }
  //determining argument of --color-ascii3 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_color_ascii3); // calling compareTwoString() function.
  if (compare == 0)
  {
   c3=argv[t+1];
  }
 }
 //determining argument of -c3 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_c3); // calling compareTwoString() function.
  if (compare == 0)
  {
   c3=argv[t+1];
  }
 }
  //determining argument of --ascii1 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_ascii1); // calling compareTwoString() function.
  if (compare == 0)
  {
   a1=argv[t+1];
  }
 }
 //determining argument of -a1 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_a1); // calling compareTwoString() function.
  if (compare == 0)
  {
   a1=argv[t+1];
  }
 }
  //determining argument of --ascii2 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_ascii2); // calling compareTwoString() function.
  if (compare == 0)
  {
   a2=argv[t+1];
  }
 }
 //determining argument of -a2 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_a2); // calling compareTwoString() function.
  if (compare == 0)
  {
   a2=argv[t+1];
  }
 }
  //determining argument of --ascii3 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_ascii3); // calling compareTwoString() function.
  if (compare == 0)
  {
   a3=argv[t+1];
  }
 }
 //determining argument of -a3 flag
 	for (t = 0; t < argc; t++)
	{
  int compare = compareTwoString(argv[t], flag_a3); // calling compareTwoString() function.
  if (compare == 0)
  {
   a3=argv[t+1];
  }
 }
	m1c=get_color_code(m1,m1c);
	m2c=get_color_code(m2,m2c);
	c1c=get_color_code(c1,c1c);
	c2c=get_color_code(c2,c2c);
	c3c=get_color_code(c3,c3c);
	a1c=get_char_code(a1,a1c);
	a2c=get_char_code(a2,a2c);
	a3c=get_char_code(a3,a3c);
	speedc=get_int_code(speed,speedc);
	densec=get_int_code(dense,densec);
	speedc=130000 - 10000 * speedc;
	//get terminal lines and columns
	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);
 /* opening the file for reading */
 fptr = fopen(filename, "r");
 if (fptr == NULL)
  {
			ch=32;
  }else{
  ch = fgetc(fptr);
  //loop to get dimensions of ascii
  while (ch != EOF)
  {
			if (ch != 10)
   {
				ii++;
		  i++;
		  asciicolumn=i;
   }
   else
   {
		  i=0;
		  asciirow++;
   }
   ch = fgetc(fptr);
  }
 fclose(fptr);
}
 int READASCII[1]={ch};
if (fptr !=(NULL))
{
 //declarating & populating READASCII
 i=0;
 fptr = fopen(filename, "r");
 ch = fgetc(fptr);
 while (ch != EOF)
 {
		if (ch!=10)
  {
  	READASCII[i]=ch;
		 i++;
   }
   ch = fgetc(fptr);
  }
 fclose(fptr);
}
 //declarating & populating ASCII0
	int ASCII0[asciirow][asciicolumn];
	i=0;
	for (x=0;x<asciirow;x++)
	{
		for (y=0;y<asciicolumn;y++)
		{
			ASCII0[x][y]=READASCII[i];
			i++;
		}
	}
 //calculating margins of ascii, in order to print it centered
 columnmargins=w.ws_col - asciicolumn;
 rowmargins=w.ws_row - asciirow;
 rowmargintop=rowmargins/2;
 columnmarginleft=columnmargins/2;
	//create ASCII1
	int ASCII1[48][190];
	for (x=0;x<48;x++){for(y=0;y<190;y++){ASCII1[x][y]=32;}}//all elements blank paces
	for (x=0;x<asciirow;x++)
	{
		for (y=0;y<asciicolumn;y++)
		{
			ASCII1[rowmargintop+x][columnmarginleft+y]=ASCII0[x][y];
		}
	}
//get terminal lines and columns
int loop;
int row=48,col=190; //max values
int DART[row][col];
for (x=0;x<row;x++)	{for (y=0;y<col;y++)	{DART[x][y]=32;}}//fill matrix with white spaces
//inf loop
loop=1;
while ( loop>0 )
{usleep(speedc);
	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);
	if ( row != w.ws_row || col != w.ws_col )
	{
		for (x=0;x<row;x++)	{for (y=0;y<col;y++)	{DART[x][y]=32;}}//fill matrix with white spaces
		for (x=0;x<row;x++)	{for (y=0;y<col;y++)	{ASCII1[x][y]=32;}}//fill matrix with white spaces
	 columnmargins=w.ws_col - asciicolumn;
	 rowmargins=w.ws_row - asciirow;
	 rowmargintop=rowmargins/2;
	 columnmarginleft=columnmargins/2;
		for (x=0;x<asciirow;x++)
		{
			for (y=0;y<asciicolumn;y++)
			{
				ASCII1[rowmargintop+x][columnmarginleft+y]=ASCII0[x][y];
			}
		}
		system("clear");
		usleep(50000);
		row=w.ws_row;
		col=w.ws_col;
		int DART[row][col];
		//draw first linew.ws_col
		for (y=0;y<col;y++)
		{
			DART[0][y]=rand() % (90 - densec * 3) + 97 ;
			//the higher the 1st value gets, more spaces appear
			if (DART[0][y]>122) {DART[0][y]=32;}
		}
	} //if
	for (x=row-1;x>0;x--)
	{
		for (y=0;y<w.ws_col;y++)
		{
			if ( DART[x][y] == 32 && DART[x-1][y] != 32 && DART[x-2][y] == 32 )
				{
					DART[x-1][y]=32;}//get rid of single letters
					if ( DART[x][y] == 32 && DART[x-1][y] != 32)
						{
							DART[x][y]=rand() % 26 + 97;
						}
						else
						{
							if ( DART[x][y] != 32 && DART[x-1][y] == 32)
								{DART[x][y]=32;}
						}
					}
				}
				//rewrite first line
				for (y=0;y<w.ws_col;y++)
				{
					if (densec==9){DART[0][y]=rand() % 30 + 97;if (DART[0][y]>122 ){DART[0][y]=32;}}
					else
					{
						if ( DART[0][y] == 32 ) {DART[0][y]=rand() % (10000 / (densec * 2 + 1)) + 97;}
						else {DART[0][y]=rand() % 30 + 97;}
						if (DART[0][y]>122 ){DART[0][y]=32;}
					}
				}
				//print matrix
				system("clear");
for (x=0;x+1<w.ws_row;x++)
	{
		for (y=0;y<w.ws_col;y++)
		{
			if(ASCII1[x][y] != 32)
			{
				a1c=get_char_code(a1,a1c);
				a2c=get_char_code(a2,a2c);
				a3c=get_char_code(a3,a3c);
				if (ASCII1[x][y] == 49){printf("\x1B[1m\x1b[%s8;5;%dm%c\x1B[0m",invert,c1c,a1c);}
				if (ASCII1[x][y] == 50){printf("\x1B[1m\x1b[%s8;5;%dm%c\x1B[0m",invert,c2c,a2c);}
				if (ASCII1[x][y] == 51){printf("\x1B[1m\x1b[%s8;5;%dm%c\x1B[0m",invert,c3c,a3c);}
				//{printf("\x1B[1m\x1b[38;5;249m%c\x1B[0m",rand() % 26 + 65);}
					}
				else
				{
					if (DART[x][y] != 32 && DART[x+1][y] ==32 )
						{printf("\x1b[38;5;%dm%c\x1B[0m",m1c,DART[x][y]);}
					else {printf("\x1b[38;5;%dm%c",m2c,DART[x][y]);}
					}
				}
 		printf("\n");
		}
	} //inf loop
	return 0;
}
// Comparing both the strings using pointers
//code as found in https://www.scaler.com/topics/c/string-comparison-in-c/
int compareTwoString(char *a, char *b)
{
    int flag = 0;
    while (*a != '\0' && *b != '\0') // while loop
    {
        if (*a != *b)
        {
            flag = 1;
        }
        a++;
        b++;
    }
 if(*a!='\0'||*b!='\0')
       return 1;
    if (flag == 0)
        return 0;
    else
        return 1;
}
