# ascii-matrix


This script written in the C language, will render the matrix effect in the
terminal, while rendering ascii art loaded from a txt file,
at the center of the terminal window.



## HOW TO CREATE SUITABLE ASCII-ART txt FILES

This program renders ascii-art txt files that are contain these characters: " (white space), 1,2,3).

In order to produce this kind of txt files, the program **jp2a** is used ([https://github.com/cslarsen/jp2a](https://github.com/cslarsen/jp2a)).

To install this program in Debian-base OSs:

    apt-get install jp2a


To generate an ascii-art txt file from a png image, in order to use it with ascii-matrix, run the following command:

    jp2a path/to/image.png --chars=" 123" --height=20>path/to/output.txt

## INSTALL ascii-matrix

*   You can run the command:

```
git clone https://gitlab.com/christosangel/asciii-matrix.git
```

or

*   visit  [https://gitlab.com/christosangel/asciii-matrix.git]( https://gitlab.com/christosangel/asciii-matrix.git)
*   Click on the *Download* button to download the zip, then unzip, and
*    once inside the ascii-matrix directory (`cd ascii-matrix` or `cd ascii-matrix-main`)
* Compile the ascii-matrix.c, in order to create the executable:


    gcc ascii-matrix.c -Wall -o ascii-matrix

After that, copy the executable to the $PATH, so that you can it from any directory:


    cp ascii-matrix ~/.local/bin/


You are now ready to go!

## USAGE

Once you have generated the ascii-art txt file that you want to show, run:

    ascii-matrix -f path/to/ascii.txt


You may want to add the flags that you prefer in order to have an outcome of your liking.


## FLAGS

The user can either use the short or the long flag version
(i.e. ascii-matrix -h and ascii-matrix --help are the same).

| short| long | explanation|
|------|---------|---------|
|||
 |-h|--help|	Shows this help text.|
|-f|--file|	Defines the path to the ascii txt file to render in the center of the matrix window.|
|||If this flag isn't used, the **command will still render a matrix screen with no ascii art**.
|-s|--speed  |	Defines the speed of the matrix falling digits.	**Acceptable values 0-9** (default: 5).|
|-d|--dense  |	Defines the density of the matrix lines. **Acceptable values 0-9** (default: 5).|
|||
|-m1|--matrix-color1  |	Defines the color of first matrix digits (default: yellow).|
|-m2|--matrix-color2 |	Defines the color of matrix digits (default: green).|
|-c1|--ascii-color1|  The first color of the ascii art (default: blue).  |
|-c2|--ascii-color2  | The second color of the ascii art (default: red).|
|-c3|--ascii-color3  | The third color of the ascii art (default: white). |
|||	**Acceptable color values: black, maroon, green, olive, navy, purple, teal, silver, grey, red, lime, yellow, blue, fuchsia, aqua and white.** These colors are defined by the color profile of your terminal.   |
|||
|-i|--invert |	Inverts the colors of the ascii art (background-foreground).  |
|||
|-a1|--ascii1 | Defines the characters for the first category of the ascii art (default: L).|
|-a2|--ascii2 | Defines the characters for the second category of the ascii art (default: L).|
|-a3|--ascii3|Defines the characters for the third category of the ascii art (default: L).|
|||	**Acceptable -a1 -a2 -a3 values :**|
|||		**L** Random uppercase letters [A-Z]|
|||		**l** Random lowercase letters [a-z]|
|||		**n** Random numbers [0-9]|
|||		**s** Space (best used in combination with **-i** or **-d 9**)|
|||		**X** the letter **X**|
|||		**p** Random punctuation marks **'()\*+,-./**|
|||


---

---
Some sample ascii txt files were added to the repo, just to help with examples of usage.

### Example 1

    ascii-matrix

will render a matrix window with the default matrix colors.

![1.gif](gif/1.gif)


### Example 2


    ascii-matrix -f ubuntu.txt

will render the ubuntu.txt ascii, with with the default values.

![2.gif](gif/2.gif)

### Example 3

    ascii-matrix -f xfce.txt -c1 grey -c2 aqua -c3 white -a1 l -a2 n -a3 L -m1 yellow -m2 green

will render the xfce.txt ascii, with white uppercase letters, grey lowercase letters and aqua punctuation marks, in a matrix with yellow and green digits.

![3.gif](gif/3.gif)

### Example 4

    ascii-matrix -f archlinux.txt -c1 navy -c2 aqua -c3 grey -a1 s -a2 s -a3 s -i

will render the archlinux.txt ascii in spaces,with the defined colors inverted.

![4.gif](gif/4.gif)

### Example 5

    ascii-matrix -f mint1.txt -d 9 -a1 s -a2 s -a3 s

will render the mint1.txt ascii with spaces, in a fully dense lined matrix screen.

![5.gif](gif/5.gif)

### Example 6

    ascii-matrix -f fedora.txt -c1 aqua -a1 L -a2 p -a3 s  -s 8

will render the fedora.txt ascii in aqua, in a rapid matrix screen.

![7.gif](gif/6.gif)

### Example 7

    ascii-matrix -f mint2.txt -c1 grey -c2 green -a1 s -a2 s -i

will render the mint2.txt ascii in green and grey spaces inverted.

![7.gif](gif/7.gif)

### Example 8

    ascii-matrix -f elmo.txt -c1 red -c2 white -c3 black


![8.gif](gif/8.gif)

### Example 9

    ascii-matrix -f lemmy.txt -a1 s -d 9

![lemmy.gif](gif/lemmy.gif)

### Example 10

    ascii-matrix -f mastodon.txt -a1 s -d 9

![mastodon.gif](gif/mastodon.gif)

Feel free to submit your ascii art txt files, in order to enrich this small collection of txt files.
